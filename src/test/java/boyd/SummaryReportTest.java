package boyd;


import org.junit.Before;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

/**
 * Unit test for SummaryReport.
 */
public class SummaryReportTest {

    /* Class Under Test */
    private SummaryReport summarizer;

    /* Test's output file placeholder */
    private Path pathToSummaryFile;

    private static final String EXPECTED_LABEL = "Wf_GotoApprovalsManagerPage";

    private static final int EXPECTED_COUNT = 2;

    private static final double EXPECTED_AVG = 633.0d;

    /**
     * Create the test case
     *
     */
    public SummaryReportTest(  ) {
        super( );
        summarizer = new SummaryReport( );
    }

    @Before
    public void setUp( ) throws Exception {

        pathToSummaryFile = Paths.get( SummaryReportTest.class.getResource(  "/boyd/TestSummary.txt" ).toURI( ) );
        
        assertTrue( pathToSummaryFile.toFile().exists() );

    }

    /**
     * First Test
     */
    @Test
    public void testFirst() throws Exception {
        
        /* Given... */
        /* ...a path to an existing log file... */
        Path pathToLogFile = Paths.get( SummaryReportTest.class.getResource(  "/boyd/Full.txt" ).toURI() );

        assertTrue( pathToLogFile.toFile().exists() );

        /* When... */
        Optional<Map<String, IntSummaryStatistics>> summarizedLog = summarizer.summarizeLog( pathToLogFile, pathToSummaryFile );

        assertTrue( summarizedLog.isPresent( ) );

        /* Then... */
        assertTrue( pathToSummaryFile.toFile( ).exists( ) );

        List<String> summarizedRows = Files.lines( pathToSummaryFile ).collect( Collectors.toList() );

        assertFalse( summarizedRows.isEmpty( ) );

        /* TODO: verify entries in summarizedLog's Map */
        
    }

    @Test
    public void testKnownOutcome() throws Exception {

        /* Fixture */
        IntSummaryStatistics expected = new IntSummaryStatistics( );

        /* These are the two values known to exist in the control sample test data */
        expected.accept( 556 );
        expected.accept( 710 );

        /* Given... */
        /* ...a path to an existing log file... */
        Path pathToLogFile = Paths.get( SummaryReportTest.class.getResource(  "/boyd/ControlSample.0.txt" ).toURI() );

        assertTrue( pathToLogFile.toFile().exists() );

        /* When... */
        Optional<Map<String, IntSummaryStatistics>> summarizedLog = summarizer.summarizeLog( pathToLogFile, pathToSummaryFile );

        assertTrue( summarizedLog.isPresent( ) );

        /* Then... */
        assertTrue( pathToSummaryFile.toFile( ).exists( ) );

        List<String> summarizedRows = Files.lines( pathToSummaryFile ).collect( Collectors.toList() );

        assertFalse( summarizedRows.isEmpty( ) );

        Map<String, IntSummaryStatistics> summaryMap = summarizedLog.get();

        assertTrue( summaryMap.containsKey( EXPECTED_LABEL ) );

        IntSummaryStatistics actual = summaryMap.get( EXPECTED_LABEL );

        assertEquals( expected.getCount( ), actual.getCount( ) );

        assertEquals( expected.getAverage( ), actual.getAverage( ), 0.0001 );

    }
}
