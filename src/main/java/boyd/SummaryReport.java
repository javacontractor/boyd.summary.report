package boyd;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import static java.nio.file.StandardOpenOption.*;

import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.logging.Logger;


/**
 * SummaryReport parses a log file produced by JMeter. It extracts
 * a subset of test result data and produces a comma-separated list
 * of performance test results.
 */
public class SummaryReport {

    private static final Logger LOG = Logger.getLogger( "SummaryReport" );

    /**
     * Processes a log file produced by JMeter; extracting a subset of
     * test result data from the log file given by the first parameter.
     * An output file containing a comma-separated list of summarized performance
     * test results will be produced at the location given as the second parameter.
     *
     * @param pathToLogFile  A {@code java.nio.file.Path} specifying the location
     *                       of the log file to be processed.
     *                       
     * @param pathToSummaryFile A {@code java.nio.file.Path} specifying where the
     *                          summary output file should be written.
     *                          
     * @return A {@code java.util.Map} of summarized performance test results. The
     * result {@code java.util.Map}'s key is a {@java.lang.String} of the name of
     * the test step that occured. The value is a {@code java.util.IntSummaryStatistics}
     * containing the test step's associated count and average.
     */
    public Optional<Map<String, IntSummaryStatistics>> summarizeLog( Path pathToLogFile, Path pathToSummaryFile ) {

        Optional< Map<String, IntSummaryStatistics> > summary;
        assert pathToLogFile.toFile( ).exists( ) : "Couldn't find resource at path '" + pathToLogFile.toString( ) + "'";

        try ( Stream< String > rows = Files.lines( pathToLogFile ); ) {

             /* Stream.skip( n ) might exclude a data row (not the header row) in a parallel stream */
            /* exclude the header row based on it containing the "timeStamp" header */
            Map<String, IntSummaryStatistics> summaryMap = rows.filter( s -> !s.contains( "timeStamp" ) ).parallel().collect( Collectors.groupingBy( l ->{ return l.split(",")[2]; }, Collectors.summarizingInt( l ->{  String val = l.split(",")[13]; int intVal = (!"".equals( val )) && val != null ? Integer.parseInt( val ) : 0; return intVal; } ) ) ) ;

            writeSummary( pathToSummaryFile, summaryMap );

            summary = Optional.of( summaryMap );

        } catch ( IOException ioe ) {
            
            LOG.severe( ioe.getMessage( ) );
            summary = Optional.empty( );
        }
        return summary;
    }

    private void writeSummary( Path pathToSummaryFile, Map< String, IntSummaryStatistics > summaryMap ) throws IOException {
        Set<String> keys = summaryMap.keySet( );
        StringBuffer summariesBuf = new StringBuffer( "Label, Count, Average" );

        List<String> summaries = new ArrayList<>( keys.size( ) );

        summaries.add( summariesBuf.toString( ) );

        for( String key : keys ) {
            IntSummaryStatistics stats = summaryMap.get( key );
            summariesBuf = new StringBuffer( key );
            summariesBuf.append(  ", " )
                    .append(  stats.getCount( ) + ", "  )
                    .append( stats.getAverage( ) );
            summaries.add( summariesBuf.toString( ) );

        }

        Files.write( pathToSummaryFile, summaries, TRUNCATE_EXISTING );
    }

    public static void main( String[] args ) {

        StringBuffer usage = new StringBuffer( "usage: java -cp {your classpath} boyd.SummaryReport \"{fully-qualified location of input log file}\" \"{fully-qualified location to where summary should be output}\"");
        
        if( args.length < 2 || "".equals( args[0] ) || "".equals( args[1] )  ) {
            System.out.println(  usage );
            System.exit( 1 );
        }

        Path inputFile =  Paths.get( args[0] );

        Path outputFile =  Paths.get( args[1] );
        try {
            if ( !outputFile.toFile( ).exists( ) ) {
                outputFile.toFile( ).createNewFile( );
            }
        } catch ( IOException ioe ){ LOG.severe( ioe.getMessage() ); }

        SummaryReport summarizer = new SummaryReport( );

        Optional<Map<String, IntSummaryStatistics>> summarizedResult = summarizer.summarizeLog( inputFile, outputFile );

        assert summarizedResult.isPresent() : "Summary was not created";
    }
}
