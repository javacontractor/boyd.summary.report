# README #

***boyd.summary.report*** is a Java language technology demonstration project. 
The *boyd.SummaryReport* class is a command line application that parses
a JMeter log file to produce a summary of JMeter test results as a comma-separted file.


### How do I get set up? ###

* [*JDK 9*](http://www.oracle.com/technetwork/java/javase/downloads/jdk9-downloads-3848520.html) is ***required*** to both build from source and to run the app from the command line
* If building from source, it is crucial that you edit the value of `<jvm>/path/to/jdk9/bin/java</jvm>` for the maven-surefire-plugin's configuration 
  in the pom.xml..
   
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.21.0</version>
                <configuration>
                    <jvm>/path/to/jdk9/bin/java</jvm>
                </configuration>
            </plugin>

* [*Maven 3.5.x or higher*](https://maven.apache.org/download.cgi) is ***required*** to build from source.
* If you've cloned or downloaded the source, you can run the test(s) with `mvn test` at the command line within the root project directory

### How To Run The Program ###

* If you've successfully built the source or downloaded "[*/dist/boyd.summary.report-0.1.jar*](https://bitbucket.org/javacontractor/boyd.summary.report/downloads/boyd.summary.report-0.1.jar)", you can run the program from the command line like so:

        java -cp {/your/path/to/boyd.summary.report-0.1.jar} / 
             boyd.SummaryReport / "{fully-qualified location of input log file}" / 
             "{fully-qualified location to where summary should be output}" 

